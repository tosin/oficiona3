A aplicação serve de controle para o robô da materia de oficina 3, basicamente a aplicação irá enviar comando para o robô seguir em frente ou virar para a direita ou esquerda. Os comandos são colocadas em uma lista e enviados ao robô, este por sua vez irá executar os camando e ao termino da execução irá enviar uma mensagem de que terminou a execução dos comandos.

PROTOCOLO DE COMUNICAÇÂO

A aplicação irá mandar uma mensagem no seguinte formato:

"srfhe"

onde:

s - Caracter que identifica o inicio da lista de comandos;
e - Caracter que identifica o fim da lista de comandos;
r - Virar a direita (right);
l - Virar a esquerda (left);
a - Seguir em frente;

Como resposta a aplicação espera uma das três mensagens:

ARRIVED - Chegou ao destino;
INCOMPLETE - Não chegou ao destino, faltou comando;
CRASH - Houve uma colizão no percurso ou saiu do grid;

