package br.com.tosin.oficina3.ui.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import app.akexorcist.bluetotohspp.library.BluetoothState;
import br.com.tosin.oficina3.R;
import br.com.tosin.oficina3.useful.ManagerBluetooth;

public class InitialActivity extends AppCompatActivity implements View.OnClickListener {

    private Button buttonPlay;
    private Button buttonAbout;
    private Button buttonSetting;

    private static Activity activity;
    private static ManagerBluetooth managerBluetooth;

    @Override
    protected void onCreate (Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_initial);

        activity = this;

        getManagerBluetooth().initialiseBluetooth(this);

        this.initialize();

    }

    @Override
    protected void onDestroy() {
        getManagerBluetooth().destroy();
        super.onDestroy();
    }

    private void initialize() {

        TextView title = (TextView) findViewById(R.id.textView_toolbar_title);
        title.setText(getString(R.string.app_name));

        buttonPlay = (Button) findViewById(R.id.buttonPlay);
        buttonSetting = (Button) findViewById(R.id.button_setting);
        buttonAbout = (Button) findViewById(R.id.button_about);

        buttonPlay.setOnClickListener(this);
        buttonSetting.setOnClickListener(this);
        buttonAbout.setOnClickListener(this);
    }

    @Override
    public void onClick (View v) {
        if (v == buttonPlay) {
            Intent intent = new Intent(InitialActivity.this, MainActivity.class);
            startActivity(intent);
        }
        else if (v == buttonSetting) {
            Intent intent = new Intent(InitialActivity.this, SettingsActivity.class);
            startActivity(intent);
        }
        else if (v == buttonAbout) {
            Intent intent = new Intent(InitialActivity.this, AboutActivity.class);
            startActivity(intent);
        }
    }

    public static ManagerBluetooth getManagerBluetooth() {
        if (InitialActivity.managerBluetooth == null){
            managerBluetooth = new ManagerBluetooth(activity);
        }
        return managerBluetooth;
    }

    public void onActivityResult (int requestCode, int resultCode, Intent data) {
        if (requestCode == BluetoothState.REQUEST_CONNECT_DEVICE) {
            if (resultCode == Activity.RESULT_OK)
                InitialActivity.getManagerBluetooth().getBluetoothManager(this).connect(data);
        }
        else if (requestCode == BluetoothState.REQUEST_ENABLE_BT) {
            if (resultCode == Activity.RESULT_OK) {
                InitialActivity.getManagerBluetooth().getBluetoothManager(this).setupService();
                InitialActivity.getManagerBluetooth().getBluetoothManager(this).startService(BluetoothState.DEVICE_ANDROID);
            }
            else {
                Snackbar.make(getCurrentFocus(), "Bluetoot não esta ativo", Snackbar.LENGTH_SHORT).show();
            }
        }
    }
}
