package br.com.tosin.oficina3.useful;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import br.com.tosin.oficina3.R;

/**
 * Created by tosin on 06/12/15.
 */
public class SetImagens {

    private Context context;

    public SetImagens(Context context) {
        this.context = context;
    }

    public Bitmap setGoOn() {
        Bitmap bitmap = null;
        bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.go_on_pt);
        return bitmap;
    }

    public Bitmap setGoOff() {
        Bitmap bitmap = null;
        bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.go_off_pt);
        return bitmap;
    }

    public Bitmap setLeft() {
        Bitmap bitmap = null;
        bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.l);
        return bitmap;
    }

    public Bitmap setRight() {
        Bitmap bitmap = null;
        bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.r);
        return bitmap;
    }

    public Bitmap setAhead() {
        Bitmap bitmap = null;
        bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.a);
        return bitmap;
    }
}
