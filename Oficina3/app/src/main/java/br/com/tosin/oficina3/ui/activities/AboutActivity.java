package br.com.tosin.oficina3.ui.activities;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import br.com.tosin.oficina3.R;

public class AboutActivity extends AppCompatActivity {

    @Override
    protected void onCreate (Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        this.toolbar();
    }

    private void toolbar() {
        TextView title = (TextView) findViewById(R.id.textView_toolbar_title);
        title.setText(getString(R.string.title_activity_about));
    }


}
