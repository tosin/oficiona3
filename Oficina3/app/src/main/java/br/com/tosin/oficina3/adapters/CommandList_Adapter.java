package br.com.tosin.oficina3.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.List;

import br.com.tosin.oficina3.R;
import br.com.tosin.oficina3.useful.SetImagens;

/**
 * Created by Roger on 18/09/2015.
 */
public class CommandList_Adapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static List<String> commandList;
    private Context context;

    public CommandList_Adapter (Context context) {
        super();
        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder (ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View itemView = inflater.inflate(R.layout.item_command_list, viewGroup, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder (RecyclerView.ViewHolder viewHolder, int position) {

        MyViewHolder myViewHolder = (MyViewHolder) viewHolder;


        String command = getCommandList().get(position);
        if (command.equals(context.getString(R.string.COMMAND_AHEAD)))
            myViewHolder.imageView_Command.setImageBitmap(new SetImagens(context).setAhead());
        if (command.equals(context.getString(R.string.COMMAND_RIGHT)))
            myViewHolder.imageView_Command.setImageBitmap(new SetImagens(context).setRight());
        if (command.equals(context.getString(R.string.COMMAND_LEFT)))
            myViewHolder.imageView_Command.setImageBitmap(new SetImagens(context).setLeft());
    }

    @Override
    public int getItemCount () {
        return getCommandList().size();
    }

    public List<String> getCommandList () {
        return commandList;
    }

    public static void setCommandList (List<String> commandList) {
        CommandList_Adapter.commandList = commandList;
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        public ImageView imageView_Command;

        public MyViewHolder (View itemView) {
            super(itemView);

            imageView_Command = (ImageView) itemView.findViewById(R.id.imageView_item_commandLis);
        }


    }
}
