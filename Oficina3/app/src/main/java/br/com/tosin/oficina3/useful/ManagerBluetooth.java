package br.com.tosin.oficina3.useful;

import android.app.Activity;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.os.Message;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.widget.Toast;

import java.util.List;

import app.akexorcist.bluetotohspp.library.BluetoothSPP;
import app.akexorcist.bluetotohspp.library.BluetoothState;
import app.akexorcist.bluetotohspp.library.DeviceList;
import br.com.tosin.oficina3.R;
import br.com.tosin.oficina3.ui.activities.MainActivity;

/**
 * Created by Roger on 27/09/2015.
 */
public class ManagerBluetooth {

    private BluetoothSPP bt;
//    private Activity activity;

    private ProgressDialog progressDialog;

    public ManagerBluetooth (Activity activity) {
        this.bt = new BluetoothSPP(activity);
//        this.activity = activity;
    }

    /**
     * Verifica se o bluetooth esta conectado, se sim disconectar se não abre a activity para encontrar uma conexão.
     */
    public void connectBluetooth(boolean isSetting, Activity activity) {
        Message msg = new Message();
        if (bt.getServiceState() == BluetoothState.STATE_CONNECTED) {
            bt.disconnect();
            msg.arg1 = 0;
        } else {
            Intent intent = new Intent(activity, DeviceList.class);
            activity.startActivityForResult(intent, BluetoothState.REQUEST_CONNECT_DEVICE);
        }
        if (!isSetting)
            MainActivity.handler.sendMessage(msg);
    }

    /**
     * Notifica se o bluetooth esta desativado e implementa os listeners de recebimento de dados e de estato da conexão.
     */
    public void initialiseBluetooth (final Activity activity) {

        if (!bt.isBluetoothAvailable()) {
            CoordinatorLayout snackbarCoordinatorLayout = (CoordinatorLayout) activity.findViewById(R.id.snackbarCoordinatorLayout);

//            Snackbar.make(snackbarCoordinatorLayout, "Bluetoot não esta ativado", Snackbar.LENGTH_SHORT).show();
            Toast.makeText(activity, "Bluetoot não esta ativado", Toast.LENGTH_LONG).show();
        }

        bt.setOnDataReceivedListener(new BluetoothSPP.OnDataReceivedListener() {
            public void onDataReceived(byte[] data, String message) {
//                Snackbar.make(activity.getCurrentFocus(), "Dado recebido: " + message, Snackbar.LENGTH_SHORT).show();

                if (progressDialog != null)
                    progressDialog.dismiss();

                Message msg = new Message();
                msg.arg1 = 1;
                msg.obj = message;
                MainActivity.handler.sendMessage(msg);

                MainActivity.setNotSending();

            }
        });

        bt.setBluetoothConnectionListener(new BluetoothSPP.BluetoothConnectionListener() {
            public void onDeviceConnected (String name, String address) {
                CoordinatorLayout snackbarCoordinatorLayout = (CoordinatorLayout) activity.findViewById(R.id.snackbarCoordinatorLayout);
//                Snackbar.make(snackbarCoordinatorLayout, "Conectado à " + name + "\n" + " endereço " + address, Snackbar.LENGTH_SHORT).show();
                Toast.makeText(activity, "Conectado à " + name + "\n" + " endereço " + address, Toast.LENGTH_LONG).show();

                Message msg = new Message();
                msg.arg1 = 1;
                MainActivity.handler.sendMessage(msg);

                MainActivity.setNotSending();

            }

            public void onDeviceDisconnected () {
                CoordinatorLayout snackbarCoordinatorLayout = (CoordinatorLayout) activity.findViewById(R.id.snackbarCoordinatorLayout);
//                Snackbar.make(snackbarCoordinatorLayout, "Conexão perdida", Snackbar.LENGTH_SHORT).show();
                Toast.makeText(activity, "Conexão perdida", Toast.LENGTH_LONG).show();

                bt.disconnect();

                if (progressDialog != null)
                    progressDialog.dismiss();

                Message msg = new Message();
                msg.arg1 = 0;
                MainActivity.handler.sendMessage(msg);

                MainActivity.setNotSending();
            }

            public void onDeviceConnectionFailed () {
                CoordinatorLayout snackbarCoordinatorLayout = (CoordinatorLayout) activity.findViewById(R.id.snackbarCoordinatorLayout);

//                Snackbar.make(snackbarCoordinatorLayout, "Incapaz de conectar", Snackbar.LENGTH_SHORT).show();
                Toast.makeText(activity, "Incapaz de conectar", Toast.LENGTH_LONG).show();

                bt.disconnect();

                if (progressDialog != null)
                    progressDialog.dismiss();

                Message msg = new Message();
                msg.arg1 = 0;
                MainActivity.handler.sendMessage(msg);

                MainActivity.setNotSending();
            }
        });
    }

    private void configLoading(Activity activity) {
        progressDialog = new ProgressDialog(activity);
        progressDialog.setIndeterminate(true);
        progressDialog.setTitle("");
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Aguardando a resposta do robô");
    }

    /**
     * Send message
     */
    public void sendMessage(List<String> list, Activity activity){


        String information = "";
        information += "s";

        for (String item : list){
            information += item.substring(0,1);
        }
        information += "e";

//        Snackbar.make(activity.getCurrentFocus(), "send: " + information, Snackbar.LENGTH_SHORT).show();

        byte[] byteArray = information.getBytes();

        if (bt.getServiceState() == BluetoothState.STATE_CONNECTED){

            configLoading(activity);
            progressDialog.show();

            MainActivity.setSending();
            bt.send(byteArray, true);
        }
        else{
            CoordinatorLayout snackbarCoordinatorLayout = (CoordinatorLayout) activity.findViewById(R.id.snackbarCoordinatorLayout);

//            Snackbar.make(snackbarCoordinatorLayout, "Não esta conectado", Snackbar.LENGTH_SHORT).show();
            Toast.makeText(activity, "Não esta conectado", Toast.LENGTH_LONG).show();

        }
    }

    public void start(Activity activity) {
        if (!bt.isBluetoothEnabled()) {
            Intent intent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            activity.startActivityForResult(intent, BluetoothState.REQUEST_ENABLE_BT);
        } else {
            if(!bt.isServiceAvailable()) {
                bt.setupService();
                // AQUI A TAG DEVICE_OTHER DEFINE SE SERÁ CONEXAO ENTRE ANDROID-ANDROID OU ANDROID-OUTRA_COISA
                bt.startService(BluetoothState.DEVICE_OTHER);
//                setup();
            }
        }
    }
    public void destroy(){
        bt.stopService();
    }

    public boolean isConnected() {
        return bt.getServiceState() == BluetoothState.STATE_CONNECTED ? true : false;
    }


    public BluetoothSPP getBluetoothManager (Activity activity) {
        if(bt == null)
            bt = new BluetoothSPP(activity);
        return bt;
    }
}
