package br.com.tosin.oficina3.dialogs;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import br.com.tosin.oficina3.R;

/**
 * Created by Roger on 07/10/2015.
 */
public class CustomDialog extends DialogFragment {

//    private boolean happy;
    public static int COMPLETE = 123;
    public static int INCOMPLETE = 456;
    public static int ROBOT_CRASH = 789;
    public static int ERROR  = 171;

    private int option;

    public CustomDialog (int option) {
        this.option = option;
    }

    @Override
    public Dialog onCreateDialog (Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog_favorite_supplier construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater li = LayoutInflater.from(getActivity());

        View view = li.inflate(R.layout.dialog_fragment, null);

        String msg = "";

        Vibrator vibrator = (Vibrator) getActivity().getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
//        vibrator.vibrate(500);

        if (option == COMPLETE){
            msg = "Parabéns! Você consguiu fugir da carrocinha!";
            long[] pattern = {0, 100, 1000, 300, 200, 100, 500, 200, 100};
            vibrator.vibrate(pattern, -1);
        }
        else if (option == INCOMPLETE){
            msg = "Ainda não chegou! Tente mais comandos!";
            vibrator.vibrate(1000);
        }
        else if (option == ROBOT_CRASH){
            msg = "Ah não! A carroconha te pegou!";
            vibrator.vibrate(1000);
        }
        else{
            msg = "O robô terminou a execução";
        }

        final TextView textView = (TextView) view.findViewById(R.id.textView_dialog_message);

        textView.setText(msg);

        builder.setView(view)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick (DialogInterface dialog, int id) {
                        //Do an action to de postive button


                    }
                });

        // Create the AlertDialog object and return it
        return builder.create();
    }
}
