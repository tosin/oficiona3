package br.com.tosin.oficina3.ui.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import app.akexorcist.bluetotohspp.library.BluetoothState;
import br.com.tosin.oficina3.R;

public class SettingsActivity extends AppCompatActivity implements Switch.OnCheckedChangeListener, View.OnClickListener {

    private Switch aSwitchVibrate;
    private Button buttonConnect;

    @Override
    protected void onCreate (Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        this.initialize();


    }

    private void initialize() {

        TextView title = (TextView) findViewById(R.id.textView_toolbar_title);
        title.setText(getString(R.string.title_activity_settings));

        buttonConnect = (Button) findViewById(R.id.button_settings_connect_connect);
        buttonConnect.setOnClickListener(this);

        aSwitchVibrate = (Switch) findViewById(R.id.switch_about_vibrate);

        aSwitchVibrate.setOnCheckedChangeListener(this);

        SharedPreferences sharedpreferences = getSharedPreferences(getString(R.string.app_name), Context.MODE_PRIVATE);
        boolean vibrate = sharedpreferences.getBoolean(getString(R.string.PREFERENCES_NAME), true);

        aSwitchVibrate.setChecked(vibrate);

    }


    @Override
    public void onCheckedChanged (CompoundButton buttonView, boolean isChecked) {
        if (buttonView == aSwitchVibrate) {
//            if (isChecked) {
//                Vibrator vibrator = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
//                long[] pattern = {0, 100, 1000, 300, 200, 100, 500, 200, 100};
//                vibrator.vibrate(pattern, -1);
//            }

            SharedPreferences sharedpreferences = getSharedPreferences(getString(R.string.app_name), Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedpreferences.edit();
            editor.putBoolean(getString(R.string.PREFERENCES_NAME), isChecked);
            editor.commit();
        }
    }

    @Override
    public void onClick(View v) {
        if (v == buttonConnect){
            InitialActivity.getManagerBluetooth().connectBluetooth(true, this);
        }
    }

    public void onActivityResult (int requestCode, int resultCode, Intent data) {
        if (requestCode == BluetoothState.REQUEST_CONNECT_DEVICE) {
            if (resultCode == Activity.RESULT_OK)
                InitialActivity.getManagerBluetooth().getBluetoothManager(this).connect(data);
        }
        else if (requestCode == BluetoothState.REQUEST_ENABLE_BT) {
            if (resultCode == Activity.RESULT_OK) {
                InitialActivity.getManagerBluetooth().getBluetoothManager(this).setupService();
                InitialActivity.getManagerBluetooth().getBluetoothManager(this).startService(BluetoothState.DEVICE_ANDROID);
            }
            else {
                Snackbar.make(getCurrentFocus(), "Bluetoot não esta ativo", Snackbar.LENGTH_SHORT).show();
            }
        }
    }
}
