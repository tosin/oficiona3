package br.com.tosin.oficina3.ui.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import app.akexorcist.bluetotohspp.library.BluetoothState;
import br.com.tosin.oficina3.R;
import br.com.tosin.oficina3.adapters.CommandList_Adapter;
import br.com.tosin.oficina3.dialogs.CustomDialog;
import br.com.tosin.oficina3.useful.Immersive;
import br.com.tosin.oficina3.useful.SetImagens;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {


    private static CommandList_Adapter commandListAdapter;

    private static List<String> commandList;
    private static FloatingActionButton floatingConnection;
    private static Activity activity;

    private static ImageView imageView_go;

    private ImageView imageView_Ahead;
    private ImageView imageView_Right;
    private ImageView imageView_Left;

    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;

    private FloatingActionButton floatingClear;
    private FloatingActionButton floatingUndo;



    private Immersive immersive;

    private static boolean sending = false;

    @Override
    protected void onCreate (Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_main);

        immersive = new Immersive(this);

        this.initialise();

        activity = this;


//        InitialActivity.getManagerBluetooth().connectBluetooth();
//        InitialActivity.getManagerBluetooth().initialiseBluetooth();

    }

    public void onStart() {
        super.onStart();
        InitialActivity.getManagerBluetooth().start(this);
    }


    //    public void onDestroy() {
//        super.onDestroy();
//        InitialActivity.getManagerBluetooth().destroy();
//    }


    /**
     * Inicializa os componentes da activity, atribui os listener dos botões e cria o RecyclerView.
     */
    private void initialise () {
        // Initializing views.
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_commandList);

        // If the size of views will not change as the data changes.
        mRecyclerView.setHasFixedSize(true);

        // Setting the LayoutManager.
        mLayoutManager = new LinearLayoutManager(activity);
        mRecyclerView.setLayoutManager(mLayoutManager);

        // Setting the adapter.
        commandListAdapter = new CommandList_Adapter(this);

        // ADD LIST IN RECYCLER
        commandListAdapter.setCommandList(getCommandList());

        mRecyclerView.setAdapter(commandListAdapter);

        imageView_Ahead = (ImageView) findViewById(R.id.imageView_ahead);
        imageView_Left = (ImageView) findViewById(R.id.imageView_left);
        imageView_Right = (ImageView) findViewById(R.id.imageView_right);
        imageView_go = (ImageView) findViewById(R.id.imageView_go);

        imageView_Ahead.setOnClickListener(this);
        imageView_Left.setOnClickListener(this);
        imageView_Right.setOnClickListener(this);
        imageView_go.setOnClickListener(this);

        imageView_go.setImageBitmap(new SetImagens(this).setGoOff());

        floatingClear = (FloatingActionButton) findViewById(R.id.floatingClear);
        floatingConnection = (FloatingActionButton) findViewById(R.id.floatingConnection);
        floatingUndo = (FloatingActionButton) findViewById(R.id.floatingBack);

        floatingUndo.setVisibility(View.GONE);
        floatingClear.setVisibility(View.GONE);

        floatingConnection.setOnClickListener(this);
        floatingClear.setOnClickListener(this);
        floatingUndo.setOnClickListener(this);

        if (InitialActivity.getManagerBluetooth().isConnected())
            floatingConnection.setImageResource(R.drawable.bluetooth_connect);
        else
            floatingConnection.setImageResource(R.drawable.bluetooth_disconnect);
    }

    public static List<String> getCommandList () {
        if (commandList == null)
            commandList = new ArrayList<>();
        return commandList;
    }

    @Override
    public void onClick (View v) {
        // se clicar em algo e a lista for diferente de vazio habilida o envio
        boolean habilitySend = getCommandList().isEmpty() ? false : true;


        if (v == imageView_Ahead) {
            getCommandList().add(getString(R.string.COMMAND_AHEAD));
            commandListAdapter.setCommandList(getCommandList());
            commandListAdapter.notifyDataSetChanged();
            // lista != vazio, mostra icones
            floatingClear.setVisibility(View.VISIBLE);
            floatingUndo.setVisibility(View.VISIBLE);

            habilitySend = true;
            setSending();
        }
        if (v == imageView_Left) {
            getCommandList().add(getString(R.string.COMMAND_LEFT));
            commandListAdapter.setCommandList(getCommandList());
            commandListAdapter.notifyDataSetChanged();

            // lista != vazio, mostra icones
            floatingClear.setVisibility(View.VISIBLE);
            floatingUndo.setVisibility(View.VISIBLE);

            habilitySend = true;
            setSending();
        }
        if (v == imageView_Right) {
            getCommandList().add(getString(R.string.COMMAND_RIGHT));
            commandListAdapter.setCommandList(getCommandList());
            commandListAdapter.notifyDataSetChanged();

            // lista != vazio, mostra icones
            floatingClear.setVisibility(View.VISIBLE);
            floatingUndo.setVisibility(View.VISIBLE);

            habilitySend = true;
            setSending();
        }

        imageView_go.setImageBitmap(habilitySend == true ? new SetImagens(activity).setGoOn() : new SetImagens(activity).setGoOff());

        if (v == imageView_go) {
            // envia via bluetooth
            if(isSending() && habilitySend) {
                InitialActivity.getManagerBluetooth().sendMessage(getCommandList(), this);
                setNotSending();
            }
            else {
                Toast.makeText(this, "Não esta conectado", Toast.LENGTH_LONG).show();
//                Snackbar.make(getCurrentFocus(), "Não esta conectado", Snackbar.LENGTH_SHORT).show();
            }
        }

        if (v == floatingClear){
            getCommandList().clear();
            commandListAdapter.setCommandList(getCommandList());
            commandListAdapter.notifyDataSetChanged();

            // lista == vazio, esconde icones
            floatingClear.setVisibility(View.GONE);
            floatingUndo.setVisibility(View.GONE);

            imageView_go.setImageBitmap(new SetImagens(activity).setGoOff());
            setNotSending();
        }
        if (v == floatingConnection){
            InitialActivity.getManagerBluetooth().connectBluetooth(false, this);
        }
        if (v == floatingUndo){
            if (!getCommandList().isEmpty()){
                getCommandList().remove(getCommandList().size() - 1);
                commandListAdapter.notifyDataSetChanged();

                if (getCommandList().isEmpty()){
                    // lista == vazio, esconde icones
                    floatingClear.setVisibility(View.GONE);
                    floatingUndo.setVisibility(View.GONE);

                    imageView_go.setImageBitmap(new SetImagens(activity).setGoOff());
                    setNotSending();
                }
            }
        }

        // mostrar o ultimo item da lista
        mRecyclerView.scrollToPosition(getCommandList().size() - 1);
    }

    public static Handler handler = new Handler(){
        @Override
        public void handleMessage (Message msg) {
            super.handleMessage(msg);
            if(msg.obj != null ){
                String text = (String) msg.obj;

                getCommandList().clear();
                commandListAdapter.notifyDataSetChanged();
                imageView_go.setImageBitmap(new SetImagens(activity).setGoOff());

                if (text.contains("ARRIVED")){
                    new CustomDialog(CustomDialog.COMPLETE).show(activity.getFragmentManager(), "missiles");
                }
                else if (text.contains("INCOMPLETE")){
                    new CustomDialog(CustomDialog.INCOMPLETE).show(activity.getFragmentManager(), "missiles");
                }
                else if (text.contains("CRASH")){
                    new CustomDialog(CustomDialog.ROBOT_CRASH).show(activity.getFragmentManager(), "missiles");
                }
                else {
                    new CustomDialog(CustomDialog.ERROR).show(activity.getFragmentManager(), "missiles");
                }
                setSending();
            }
            else {
                if (msg.arg1 == 0) {
                    setNotSending();
                    floatingConnection.setImageResource(R.drawable.bluetooth_disconnect);
                }
                if (msg.arg1 == 1) {
                    setSending();
                    floatingConnection.setImageResource(R.drawable.bluetooth_connect);
                }
            }
        }
    };


    public void onActivityResult (int requestCode, int resultCode, Intent data) {
        if (requestCode == BluetoothState.REQUEST_CONNECT_DEVICE) {
            if (resultCode == Activity.RESULT_OK)
                InitialActivity.getManagerBluetooth().getBluetoothManager(this).connect(data);
        }
        else if (requestCode == BluetoothState.REQUEST_ENABLE_BT) {
            if (resultCode == Activity.RESULT_OK) {
                InitialActivity.getManagerBluetooth().getBluetoothManager(this).setupService();
                InitialActivity.getManagerBluetooth().getBluetoothManager(this).startService(BluetoothState.DEVICE_ANDROID);
            }
            else {
                Toast.makeText(this, "Bluetoot não esta ativo", Toast.LENGTH_LONG).show();
//                Snackbar.make(getCurrentFocus(), "Bluetoot não esta ativo", Snackbar.LENGTH_SHORT).show();
            }
        }
    }

    public static boolean isSending(){
        return sending;
    }

    public static void setSending(){
        sending = true;
        imageView_go.setImageBitmap(new SetImagens(activity).setGoOff());
    }

    public static void setNotSending(){
        sending = false;
        if (!getCommandList().isEmpty())
            imageView_go.setImageBitmap(new SetImagens(activity).setGoOn());
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

        immersive.onWindowFocusChanged(hasFocus);

    }

}

